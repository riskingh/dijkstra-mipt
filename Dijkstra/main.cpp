//
//  main.cpp
//  Dijkstra
//
//  Created by Максим Гришкин on 25/05/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#include <iostream>

#include "CommonGraph.h"
#include "ClassicGraph.h"
#include "BusScheduleGraph.h"
#include "WeightLimitationGraph.h"
#include "Dijkstra.h"
#include "FordBellman.h"

using namespace NDijkstra;

void solveClassic() {
//    http://informatics.mccme.ru/mod/statements/view3.php?id=10845&chapterid=5#1
    int n, s, f;
    std::cin >> n >> s >> f;
    s--; f--;
    
    std::vector<std::vector<CClassicEdgeEnd>> adj(n);
    
    int i, j, w;
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            std::cin >> w;
            if (w != -1)
                adj[i].push_back(CClassicEdgeEnd(j, w));
        }
    }
    
    CClassicGraph graph(adj);
    std::vector<long long> distances = dijkstra(s, &graph);
    std::cout << distances[f] << "\n";
}

void solveBus() {
//    http://informatics.mccme.ru/mod/statements/view3.php?id=10845&chapterid=170#1
    int n, d, v;
    std::cin >> n >> d >> v;
    d--; v--;
    int r;
    std::cin >> r;
    
    std::vector<std::vector<CBusScheduleEdgeEnd>> adj(n);
    int a, b, c, d1;
    for (int i = 0; i < r; ++i) {
        std::cin >> a >> b >> c >> d1;
        a--; c--;
        adj[a].push_back(CBusScheduleEdgeEnd(c, b, d1));
    }
    
    CBusScheduleGraph graph(adj);
    std::vector<long long> distances = dijkstra(d, &graph);
    std::cout << distances[v] << "\n";
}

void solveWeightLimitation() {
//    http://informatics.mccme.ru/mod/statements/view3.php?id=10845&chapterid=1967#1
    int n, m;
    std::cin >> n >> m;
    int i, a, b, c, d;
    
    std::vector<std::vector<CWeightLimitationEdgeEnd>> adj(n);
    for (i = 0; i < m; ++i) {
        std::cin >> a >> b >> c >> d;
        a--; b--;
        adj[a].push_back(CWeightLimitationEdgeEnd(b, c, d - 3000000));
        adj[b].push_back(CWeightLimitationEdgeEnd(a, c, d - 3000000));
    }
    
    std::vector<long long> distances;
    CWeightLimitationGraph graph(0, adj);
    int left = 0, middle, right = 10000001;
    while (right - left > 1) {
        middle = (left + right) / 2;
        graph.setCarWeight(middle * 100);
        distances = dijkstra(0, &graph);
        if (distances[n - 1] != -1 && distances[n - 1] <= 1440)
            left = middle;
        else
            right = middle;
    }
    std::cout << left << "\n";
}

int main(int argc, const char * argv[]) {
    return 0;
}
