//
//  Dijkstra.h
//  Dijkstra
//
//  Created by Максим Гришкин on 25/05/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#ifndef __Dijkstra__Dijkstra__
#define __Dijkstra__Dijkstra__

#include <stdio.h>
#include <vector>
#include <set>
#include "CommonGraph.h"

namespace NDijkstra {
    template <class _WeightType, class _EdgeEndConstIterator>
    std::vector<_WeightType> dijkstra(unsigned int _begin, ICommonGraph<_WeightType, _EdgeEndConstIterator> *_graph) {
        std::vector<_WeightType> distance(_graph->getVertexCount(), _graph->getBadValue());
        distance[_begin] = _graph->getZeroValue();
        
        std::set<std::pair<_WeightType, unsigned int>> processedVertexes;
        processedVertexes.insert(std::make_pair(distance[_begin], _begin));
        
        _WeightType currentDistance, newDistance;
        unsigned int currentVertex, nextVertex;
        while (!processedVertexes.empty()) {
            currentDistance = processedVertexes.begin()->first;
            currentVertex = processedVertexes.begin()->second;
            processedVertexes.erase(processedVertexes.begin());
            for (_EdgeEndConstIterator edgeEndIterator = _graph->adjacentBegin(currentVertex); edgeEndIterator != _graph->adjacentEnd(currentVertex); ++edgeEndIterator) {
                newDistance = _graph->addDistance(currentDistance, currentVertex, *edgeEndIterator);
                nextVertex = _graph->getEndVertexNumber(edgeEndIterator);
                if (newDistance != _graph->getBadValue() && (distance[nextVertex] == _graph->getBadValue() || newDistance < distance[nextVertex])) {
                    processedVertexes.erase(std::make_pair(distance[nextVertex], nextVertex));
                    distance[nextVertex] = newDistance;
                    processedVertexes.insert(std::make_pair(distance[nextVertex], nextVertex));
                }
            }
        }
        
        return distance;
    }
}

#endif /* defined(__Dijkstra__Dijkstra__) */
