//
//  ClassicGraph.h
//  Dijkstra
//
//  Created by Максим Гришкин on 25/05/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#ifndef __Dijkstra__ClassicGraph__
#define __Dijkstra__ClassicGraph__

#include <stdio.h>
#include <vector>

#include "CommonGraph.h"

namespace NDijkstra {
    struct CClassicEdgeEnd {
        int vertexNumber;
        long long weight;
        CClassicEdgeEnd(int _vertexNumber, long long _weight)
        : vertexNumber(_vertexNumber), weight(_weight) {}
    };
    
    class CClassicGraph: public ICommonGraph<long long, std::vector<CClassicEdgeEnd>::const_iterator> {
    private:
        typedef ICommonGraph<long long, std::vector<CClassicEdgeEnd>::const_iterator> BaseClass;
        std::vector<std::vector<CClassicEdgeEnd>> adjacentVertexes;
    public:
        CClassicGraph(const std::vector<std::vector<CClassicEdgeEnd>> &_ajacentVertexes)
        : adjacentVertexes(_ajacentVertexes) {
            BaseClass::zeroValue = 0;
            BaseClass::badValue = -1;
        }
        
        ~CClassicGraph() {
            adjacentVertexes.clear();
        }
        
        unsigned int getVertexCount() const {
            return (unsigned int)adjacentVertexes.size();
        }
        
        unsigned int getEndVertexNumber(std::vector<CClassicEdgeEnd>::const_iterator _edgeEndConstIterator) const {
            return _edgeEndConstIterator->vertexNumber;
        }

        long long weight(CClassicEdgeEnd _edgeEnd) const {
            return _edgeEnd.weight;
        }
        
        std::vector<CClassicEdgeEnd>::const_iterator adjacentBegin(int _vertex) const {
            return adjacentVertexes[_vertex].cbegin();
        }
        std::vector<CClassicEdgeEnd>::const_iterator adjacentEnd(int _vertex) const {
            return adjacentVertexes[_vertex].cend();
        }
        
        long long addDistance(long long _firstVertexDistance, int _firstVertex, const CClassicEdgeEnd &_edgeEnd) const {
            return _firstVertexDistance + _edgeEnd.weight;
        }
    };
}

#endif /* defined(__Dijkstra__ClassicGraph__) */
