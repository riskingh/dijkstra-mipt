//
//  CommonGraph.h
//  Dijkstra
//
//  Created by Максим Гришкин on 25/05/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#ifndef __Dijkstra__CommonGraph__
#define __Dijkstra__CommonGraph__

#include <stdio.h>

namespace NDijkstra {
    template <class _WeightType, class _EdgeEndConstIterator>
    class ICommonGraph {
    private:
    protected:
        _WeightType zeroValue;
        _WeightType badValue;
    public:
        virtual ~ICommonGraph() {}
        
        virtual unsigned int getVertexCount() const = 0;
        
        virtual _WeightType getZeroValue() const {
            return zeroValue;
        }
        virtual _WeightType getBadValue() const {
            return badValue;
        }
        
        virtual unsigned int getEndVertexNumber(_EdgeEndConstIterator _edgeEndConstIterator) const = 0;
        
        virtual _WeightType weight(typename std::iterator_traits<_EdgeEndConstIterator>::value_type _edgeEnd) const = 0;
        
        virtual _EdgeEndConstIterator adjacentBegin(int _vertex) const = 0;
        virtual _EdgeEndConstIterator adjacentEnd(int _vertex) const = 0;
        
        virtual _WeightType addDistance(_WeightType _firstVertexDistance, int _firstVertex, const typename std::iterator_traits<_EdgeEndConstIterator>::value_type &_edgeEnd) const = 0;
    };
}

#endif /* defined(__Dijkstra__CommonGraph__) */
