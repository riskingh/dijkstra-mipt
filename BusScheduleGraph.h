//
//  BusScheduleGraph.h
//  Dijkstra
//
//  Created by Максим Гришкин on 25/05/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#ifndef __Dijkstra__BusScheduleGraph__
#define __Dijkstra__BusScheduleGraph__

#include <stdio.h>
#include <vector>

#include "CommonGraph.h"

namespace NDijkstra {
    struct CBusScheduleEdgeEnd {
        int vertexNumber;
        long long timeBegin, timeEnd;
        CBusScheduleEdgeEnd(int _vertexNumber, long long _timeBegin, long long _timeEnd)
        : vertexNumber(_vertexNumber), timeBegin(_timeBegin), timeEnd(_timeEnd) {}
    };
    
    class CBusScheduleGraph: public ICommonGraph<long long, std::vector<CBusScheduleEdgeEnd>::const_iterator> {
    private:
        typedef ICommonGraph<long long, std::vector<CBusScheduleEdgeEnd>::const_iterator> BaseClass;
        std::vector<std::vector<CBusScheduleEdgeEnd>> adjacentVertexes;
    public:
        CBusScheduleGraph(const std::vector<std::vector<CBusScheduleEdgeEnd>> &_ajacentVertexes)
        : adjacentVertexes(_ajacentVertexes) {
            BaseClass::zeroValue = 0;
            BaseClass::badValue = -1;
        }
        
        ~CBusScheduleGraph() {
            adjacentVertexes.clear();
        }
        
        unsigned int getVertexCount() const {
            return (unsigned int)adjacentVertexes.size();
        }
        
        unsigned int getEndVertexNumber(std::vector<CBusScheduleEdgeEnd>::const_iterator _edgeEndConstIterator) const {
            return _edgeEndConstIterator->vertexNumber;
        }

        long long weight(CBusScheduleEdgeEnd _edgeEnd) const {
            return _edgeEnd.timeEnd - _edgeEnd.timeBegin;
        }
        
        std::vector<CBusScheduleEdgeEnd>::const_iterator adjacentBegin(int _vertex) const {
            return adjacentVertexes[_vertex].cbegin();
        }
        
        std::vector<CBusScheduleEdgeEnd>::const_iterator adjacentEnd(int _vertex) const {
            return adjacentVertexes[_vertex].cend();
        }
        
        long long addDistance(long long _firstVertexDistance, int _firstVertex, const CBusScheduleEdgeEnd &_edgeEnd) const {
            if (_firstVertexDistance <= _edgeEnd.timeBegin)
                return _edgeEnd.timeEnd;
            return BaseClass::badValue;
        }

    };
}

#endif /* defined(__Dijkstra__BusScheduleGraph__) */
