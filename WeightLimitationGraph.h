//
//  WeightLimitationGraph.h
//  Dijkstra
//
//  Created by Максим Гришкин on 26/05/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#ifndef __Dijkstra__WeightLimitationGraph__
#define __Dijkstra__WeightLimitationGraph__

#include <stdio.h>
#include <vector>
#include "CommonGraph.h"

namespace NDijkstra {
    struct CWeightLimitationEdgeEnd {
        unsigned int vertexNumber;
        long long passTime, maxWeight;
        CWeightLimitationEdgeEnd(unsigned int _vertexNumber, long long _passTime, long long _maxWeight)
        : vertexNumber(_vertexNumber), passTime(_passTime), maxWeight(_maxWeight) {}
    };
    
    class CWeightLimitationGraph: public ICommonGraph<long long, std::vector<CWeightLimitationEdgeEnd>::const_iterator> {
    private:
        typedef ICommonGraph<long long, std::vector<CWeightLimitationEdgeEnd>::const_iterator> BaseClass;
        static const long long zeroValue = 0;
        static const long long badValue = -1;
        long long carWeight;
        std::vector<std::vector<CWeightLimitationEdgeEnd>> adjacentVertexes;
    public:
        CWeightLimitationGraph(long long _carWeight, const std::vector<std::vector<CWeightLimitationEdgeEnd>> &_adjacentVertexes)
        : carWeight(_carWeight), adjacentVertexes(_adjacentVertexes) {
            BaseClass::zeroValue = 0;
            BaseClass::badValue = -1;
        }
        
        ~CWeightLimitationGraph() {
            adjacentVertexes.clear();
        }
        
        unsigned int getVertexCount() const {
            return (unsigned int)adjacentVertexes.size();
        }
        
        unsigned int getEndVertexNumber(std::vector<CWeightLimitationEdgeEnd>::const_iterator _edgeEndConstIterator) const {
            return _edgeEndConstIterator->vertexNumber;
        }
        
        long long weight(CWeightLimitationEdgeEnd _edgeEnd) const {
            return _edgeEnd.passTime;
        }
        
        std::vector<CWeightLimitationEdgeEnd>::const_iterator adjacentBegin(int _vertex) const {
            return adjacentVertexes[_vertex].cbegin();
        }
        std::vector<CWeightLimitationEdgeEnd>::const_iterator adjacentEnd(int _vertex) const {
            return adjacentVertexes[_vertex].cend();
        }
        
        long long addDistance(long long _firstVertexDistance, int _firstVertex, const CWeightLimitationEdgeEnd &_edgeEnd) const {
            if (carWeight <= _edgeEnd.maxWeight)
                return _firstVertexDistance + _edgeEnd.passTime;
            return BaseClass::badValue;
        }
        
        void setCarWeight(long long _newCarWeight) {
            carWeight = _newCarWeight;
        }
    };
}

#endif /* defined(__Dijkstra__WeightLimitationGraph__) */
